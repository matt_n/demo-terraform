aws lambda invoke \
    --function-name hello-matt-1 \
    --cli-binary-format raw-in-base64-out \
    --payload '{"hello":"Matt"}' response

cat response | jq
rm -f response