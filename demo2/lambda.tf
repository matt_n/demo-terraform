resource "aws_iam_role" "hello-matt-2-role" {
  name = "hello-matt-2-role"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "hello-matt-2-function" {
  filename      = "code/build.zip"
  function_name = "hello-matt-2"
  role          = aws_iam_role.hello-matt-2-role.arn
  handler       = "lambda_function.lambda_handler"
  runtime = "python3.8"
}