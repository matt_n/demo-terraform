## LAMBDA PERMISSIONS

resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.hello-matt-2-function.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = "${aws_api_gateway_rest_api.hello-matt-2-gateway.execution_arn}/*/*"
}

## API GATEWAY

resource "aws_api_gateway_rest_api" "hello-matt-2-gateway" {
  name        = "hello-matt-2"

  endpoint_configuration {
    types = [ "REGIONAL" ]
  }
}

resource "aws_api_gateway_resource" "resource" {
  rest_api_id = aws_api_gateway_rest_api.hello-matt-2-gateway.id
  parent_id   = aws_api_gateway_rest_api.hello-matt-2-gateway.root_resource_id
  path_part   = "{proxy+}"
}

## GET METHOD
resource "aws_api_gateway_method" "method-get" {
  rest_api_id   = aws_api_gateway_rest_api.hello-matt-2-gateway.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "GET"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration-get" {
rest_api_id = aws_api_gateway_rest_api.hello-matt-2-gateway.id
  resource_id = aws_api_gateway_method.method-get.resource_id
  http_method = aws_api_gateway_method.method-get.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.hello-matt-2-function.invoke_arn
}

## POST METHOD
resource "aws_api_gateway_method" "method-post" {
 rest_api_id   = aws_api_gateway_rest_api.hello-matt-2-gateway.id
  resource_id   = aws_api_gateway_resource.resource.id
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration-post" {
 rest_api_id = aws_api_gateway_rest_api.hello-matt-2-gateway.id
  resource_id = aws_api_gateway_method.method-post.resource_id
  http_method = aws_api_gateway_method.method-post.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.hello-matt-2-function.invoke_arn
}

## DEPLOYMENT

resource "aws_api_gateway_deployment" "deployment" {
  depends_on = [
    aws_api_gateway_integration.integration-get,
    aws_api_gateway_integration.integration-post,
  ]

  rest_api_id = aws_api_gateway_rest_api.hello-matt-2-gateway.id
  stage_name  = "production"
}

output "invoke_url" {
    value = aws_api_gateway_deployment.deployment.invoke_url
}