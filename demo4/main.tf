### USE TERRAFORM 0.13

## LAMBDAS

# This Lambda will put a message on the SQS queue
module "matt-post-lambda-1" {
  source      = "./modules/lambda"
  lambda_name = "matt-post-lambda-1"
}

# This lambda will get a message from the SQS queue
module "matt-get-lambda-1" {
  source      = "./modules/lambda"
  lambda_name = "matt-get-lambda-1"
}

module "matt-get-lambda-2" {
  source      = "./modules/lambda"
  lambda_name = "matt-get-lambda-2"
}

## SQS QUEUE

module "matt-queue" {
  source      = "./modules/sqs_queue"
  queue_name  = "mattn-test-queue"
}

## GIVE ACCESS TO SQS

resource "aws_iam_role_policy_attachment" "matt-post-lambda-1-sqs-attach" {
  role       = module.matt-post-lambda-1.role.name
  policy_arn = module.matt-queue.publish_policy.arn
}

resource "aws_iam_role_policy_attachment" "matt-get-lambda-1-sqs-attach" {
  role       = module.matt-get-lambda-1.role.name
  policy_arn = module.matt-queue.receive_policy.arn
}

## API GATEWAY

module "hello-matt-api" {
  source       = "./modules/apigw"
  service_name = "hello-matt-api"
}

## RESOURCES

module "test" {
  source  = "./modules/apigw_resource"
  gateway = module.hello-matt-api.gateway
  path    = "test" # /test
  depends_on = [ module.hello-matt-api ]
}

module "hello" {
  source  = "./modules/apigw_resource"
  gateway = module.hello-matt-api.gateway
  path    = "hello" # /hello
  depends_on = [ module.hello-matt-api ]
}

## METHODS

module "get-test" {
  source      = "./modules/apigw_method"
  function    = module.matt-get-lambda-1.function
  http_method = "GET"
  gateway     = module.hello-matt-api.gateway
  resource    = module.test.resource
  depends_on  = [ module.hello-matt-api, module.test ]
}

module "get-hello" {
  source      = "./modules/apigw_method"
  function    = module.matt-get-lambda-2.function
  http_method = "GET"
  gateway     = module.hello-matt-api.gateway
  resource    = module.hello.resource
  depends_on  = [ module.hello-matt-api, module.hello ]
}

module "post-test" {
  source      = "./modules/apigw_method"
  function    = module.matt-post-lambda-1.function
  http_method = "POST"
  gateway     = module.hello-matt-api.gateway
  resource    = module.test.resource
  depends_on  = [ module.hello-matt-api, module.test ]
}


## DEPLOYMENT

module "hello-matt-deployment" {
  source  = "./modules/apigw_deployment"
  gateway = module.hello-matt-api.gateway
  depends_on = [ module.hello-matt-api, module.test, module.hello, module.get-test, module.get-hello, module.post-test ]
}

output "hello-matt-api-invoke_url" {
  value = module.hello-matt-deployment.invoke_url
}