from __future__ import print_function
import json
import boto3

def lambda_handler(event, context):

    sqs = boto3.client('sqs')
    queue_url = 'https://sqs.eu-west-1.amazonaws.com/XXX/mattn-test-queue'

    response = sqs.send_message(
        QueueUrl=queue_url,
        DelaySeconds=10,
        MessageAttributes={
            'Title': {
                'DataType': 'String',
                'StringValue': 'TEST'
            }
        },
        MessageBody=(
            'This is a test'
        )
    )

    return     {
        "statusCode": 200,
        "body": json.dumps(response['MessageId'])
    }