# These deploy the lamdas that publish and receive to SQS

cp matt-get-lambda-1.py lambda_function.py
zip matt-get-lambda-1.zip lambda_function.py
rm -f lambda_function.py

cp matt-post-lambda-1.py lambda_function.py
zip matt-post-lambda-1.zip lambda_function.py
rm -f lambda_function.py

aws lambda update-function-code \
  --profile default \
  --function-name matt-get-lambda-1 \
  --zip-file fileb://matt-get-lambda-1.zip

aws lambda update-function-code \
  --profile default \
  --function-name matt-post-lambda-1 \
  --zip-file fileb://matt-post-lambda-1.zip