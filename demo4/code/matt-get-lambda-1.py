from __future__ import print_function
import json
import boto3

def lambda_handler(event, context):

    sqs = boto3.client('sqs')
    queue_url = 'https://sqs.eu-west-1.amazonaws.com/XXX/mattn-test-queue'

    response = sqs.receive_message(
        QueueUrl=queue_url,
        AttributeNames=[
            'SentTimestamp'
        ],
        MaxNumberOfMessages=1,
        MessageAttributeNames=[
            'All'
        ],
        VisibilityTimeout=0,
        WaitTimeSeconds=0
    )

    message = response['Messages'][0]
    receipt_handle = message['ReceiptHandle']

    # Delete received message from queue
    sqs.delete_message(
        QueueUrl=queue_url,
        ReceiptHandle=receipt_handle
    )

    return     {
        "statusCode": 200,
        "body": json.dumps(message)
    }