## API GATEWAY

resource "aws_api_gateway_rest_api" "gateway" {
  name = var.service_name

  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

## OUTPUTS

output "gateway" {
  value = aws_api_gateway_rest_api.gateway
}