## API GATEWAY RESOURCE

resource "aws_api_gateway_resource" "resource" {
  rest_api_id = var.gateway.id
  parent_id   = var.gateway.root_resource_id
  path_part   = var.path
}

output "resource" {
  value = aws_api_gateway_resource.resource
}