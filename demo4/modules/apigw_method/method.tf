## LAMBDA PERMISSIONS

resource "aws_lambda_permission" "lambda_permission" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = var.function.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${var.gateway.execution_arn}/*/*"
}

## METHOD and INTEGRATION
resource "aws_api_gateway_method" "method" {
  rest_api_id   = var.gateway.id
  resource_id   = var.resource.id
  http_method   = var.http_method
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "integration" {
  rest_api_id = var.gateway.id
  resource_id = aws_api_gateway_method.method.resource_id
  http_method = aws_api_gateway_method.method.http_method

  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = var.function.invoke_arn
}