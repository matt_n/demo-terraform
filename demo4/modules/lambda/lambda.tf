resource "aws_iam_role" "role" {
  name = var.lambda_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "function" {
  filename      = "${path.module}/code/build.zip"
  function_name = var.lambda_name
  role          = aws_iam_role.role.arn
  handler       = "lambda_function.lambda_handler"
  runtime       = "python3.8"
}

## OUTPUTS

output "function" {
  value = aws_lambda_function.function
}

output "role" {
  value = aws_iam_role.role
}