## DEPLOYMENT

resource "aws_api_gateway_deployment" "deployment" {
  rest_api_id = var.gateway.id
  stage_name  = "production"
}

output "invoke_url" {
  value = aws_api_gateway_deployment.deployment.invoke_url
}