data "aws_caller_identity" "current" {}

## DEADLETTER QUEUE
resource "aws_sqs_queue" "deadletter" {
  name                      = "${var.queue_name}-deadletter"
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
}

## QUEUE
resource "aws_sqs_queue" "queue" {
  name                      = var.queue_name
  delay_seconds             = 90
  max_message_size          = 2048
  message_retention_seconds = 86400
  receive_wait_time_seconds = 10
  redrive_policy = jsonencode({
    deadLetterTargetArn = aws_sqs_queue.deadletter.arn
    maxReceiveCount     = 4
  })
  depends_on            = [ aws_sqs_queue.deadletter ]
}

## PUBLISH ACCESS
resource "aws_iam_policy" "publish_policy" {
  name = "sqs-${var.queue_name}-publish"

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublishPolicy",
            "Effect": "Allow",
            "Action": "sqs:SendMessage",
            "Resource": "arn:aws:sqs:*:${data.aws_caller_identity.current.account_id}:${var.queue_name}"
        }
    ]
  }
  EOF
}

## RECEIVE ACCESS
resource "aws_iam_policy" "receive_policy" {
  name = "sqs-${var.queue_name}-receive"

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "ReceivePolicy",
            "Effect": "Allow",
            "Action": [ "sqs:ReceiveMessage", "sqs:DeleteMessage" ],
            "Resource": "arn:aws:sqs:*:${data.aws_caller_identity.current.account_id}:${var.queue_name}"
        }
    ]
  }
  EOF
}

output "publish_policy" {
    value = aws_iam_policy.publish_policy
}

output "receive_policy" {
    value = aws_iam_policy.receive_policy
}