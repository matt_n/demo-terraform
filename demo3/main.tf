module "hello-matt-3" {
  source  = "./modules/apigw_lambda"
  service_name = "hello-matt-3"
}

output "hello-matt-3-invoke_url" {
    value = module.hello-matt-3.invoke_url
}

module "hello-matt-3a" {
  source  = "./modules/apigw_lambda"
  service_name = "hello-matt-3a"
}

output "hello-matt-3a-invoke_url" {
    value = module.hello-matt-3a.invoke_url
}

### module "hello-matt-3" {
###   source = "git@github.com:hashicorp/example.git"
### }
