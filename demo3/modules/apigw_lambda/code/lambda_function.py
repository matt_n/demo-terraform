from __future__ import print_function
import json
print('Loading function')

def lambda_handler(event, context):
    return     {
        "statusCode": 200,
        "body": json.dumps('This was the lambda that ran : ' + context.function_name)
    }