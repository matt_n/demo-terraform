resource "aws_iam_role" "role" {
  name = var.service_name

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_lambda_function" "function" {
  filename      = "${path.module}/code/build.zip"
  function_name = var.service_name
  role          = aws_iam_role.role.arn
  handler       = "lambda_function.lambda_handler"
  runtime = "python3.7"
}