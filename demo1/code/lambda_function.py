from __future__ import print_function

def lambda_handler(event, context):
    return {
        'message': 'This was the lambda that ran : ' + context.function_name
    }